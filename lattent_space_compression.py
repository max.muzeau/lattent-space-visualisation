import time
import os
import sys
sys.path.append('/media/max/TOSHIBA EXT/ONERA_SONDRA/algos/le_pipe')

from pipeline.models.AAE import ConvEncoder
from pipeline.datasets.load import load_eval_data
from pipeline.datasets.datasets import test_data
from pipeline.datasets.preprocessing import normalization,ToTensor
from pipeline.utils import move_to_device, load_model, save_im, save_plot, tresh_im, normalize01

from torchvision import transforms
from torch.utils.data import DataLoader
import torch.nn as nn
import torch

import numpy as np

z_size = 128 

def load_last_model(model,path,model_name):
        epochs = filter(lambda file: file.startswith("{}_epoch_".format(model_name)), os.listdir(path))
        epochs = map(lambda file: int(file[file.find("h_")+2:]), epochs)
        epochs = list(epochs)
        last_model_path = os.path.join(path, "{}_epoch_{}".format(model_name,max(epochs)))
        load_model(model,last_model_path)
        print("{} found at epoch {}...".format(model_name,max(epochs)))

def encode_patches(encoder: nn.Module, input_data: torch.Tensor,name: str, device: str):

    patch_size = 64
    stride = 64 

    input_data = move_to_device(input_data, device=device)
    (un,c,h,w) = input_data.shape
    
    latt_vectors = np.array([]).reshape(0,z_size)

    if h == patch_size:
        x_range = list(np.array([0]))
    else:
        x_range = list(range(0,h-patch_size,stride))
        if (x_range[-1]+patch_size)<h : x_range.extend(range(h-patch_size,h-patch_size+1))
        
    if w == patch_size:
        y_range = list(np.array([0]))
    else:
        y_range = list(range(0,w-patch_size,stride))
        if (y_range[-1]+patch_size)<w : y_range.extend(range(w-patch_size,w-patch_size+1))
           
    for x in x_range:
        for y in y_range:
            z = torch.squeeze(encoder(input_data[:,:,x:x+patch_size,y:y+patch_size]))
            z = z.cpu().data.numpy().reshape(1,-1)

            latt_vectors = np.vstack([latt_vectors,z])

    return latt_vectors
            


if __name__ == "__main__":
    
    zone = 'ano'
    eval_dir = '/media/max/TOSHIBA EXT/ONERA_SONDRA/algos/visualisation/crop/{}'.format(zone)
    save_path = './latt_space/no_disc/{}.npy'.format(zone)

    model_path = '/media/max/TOSHIBA EXT/ONERA_SONDRA/algos/git_AAE/pipeline/out/AAE_CLR_no_disc'
    device = 'cuda'

    os.makedirs(os.path.dirname(save_path),exist_ok=True)

    norm = np.array([[-30,-30.7,-29.7,-30.1],[-11.0,-12.4,-12.3,-11.0]]) # Onera
    eval_paths = load_eval_data(eval_dir)
    process_eval = transforms.Compose([normalization(norm[0],norm[1]),ToTensor()])
    eval_dataset = test_data(eval_paths,process_eval)
    eval_data = DataLoader(eval_dataset)

    encoder = ConvEncoder(4).to(device)
    load_last_model(encoder,model_path,'encoder')
    encoder.eval()

    start_time = time.time()
    with torch.no_grad():
        latt_vectors = np.array([]).reshape(0,z_size)
        for step_id, (input_data,name) in enumerate(eval_data):
            vec = encode_patches(encoder,input_data,name,device)
            latt_vectors = np.vstack([latt_vectors,vec])
            predict_time = time.time() - start_time
            print('[{:.5} s] Compression step {} finished'.format(predict_time,step_id))

    np.save(save_path,latt_vectors)
    predict_time = time.time() - start_time
    print('[{:.5} s] Compression is completed. Saved in : {}'.format(predict_time,save_path))

    
