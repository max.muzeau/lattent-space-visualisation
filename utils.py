import numpy as np
import cv2
import matplotlib.pyplot as plt
import scipy.fft
import os

from torch import normal


def normalize01(im,val=None):
    if val == None:
        m = np.amin(im)
        M = np.amax(im)
    else:
        m = val[0]
        M = val[1]
    im_norm = (im-m)/(M-m)
    return im_norm

# Apply a treshold, a defined treshold or mean+3*var
def tresh_im(img,treshold=None,k=3):

    imabs = np.abs(img)
    sh = imabs.shape

    if treshold == None:
        if len(sh) == 2:
            mean = np.mean(imabs)
            std = np.std(imabs)
            treshold = mean+k*std
            imabs = np.clip(imabs,None,treshold)
            imabs = normalize01(imabs)
            print('treshold : {}'.format(treshold))

        elif len(sh) == 3:
            for i in range(sh[2]):
                im_p = imabs[:,:,i] # take channel i
                mean = np.mean(im_p)
                std = np.std(im_p)
                treshold_p = mean+k*std # compute treshold
                im_p = np.clip(im_p,None,treshold_p) # apply treshold
                im_p = normalize01(im_p) # normalize [0-1]
                imabs[:,:,i] = im_p
                print('treshold for channel {} : {}'.format(i,treshold_p))

    else:
        if len(sh) == 2:
            imabs = np.clip(imabs,None,treshold)
            imabs = normalize01(imabs)

        elif len(sh) == 3:
            for i in range(sh[2]):
                if len(treshold) == sh[2]:
                    im_p = imabs[:,:,i] # take channel i
                    im_p = np.clip(im_p,None,treshold[i]) # apply treshold
                    im_p = normalize01(im_p,[0,treshold[i]]) # normalize [0-max/treshold]
                    imabs[:,:,i] = im_p
                else:
                    print('Number of tresholds should be the same as number of channels but got {} and {}'.format(len(treshold),sh[2]))    
    return imabs

    
def plot_im(img,title = '',bar = False,save=False):
    if len(img.shape) == 2:
        plt.imshow(img, cmap = 'gray')
    else:
        plt.imshow(img)
    if bar:
        plt.colorbar()
    plt.axis('off')

    if not save:
        plt.title(title)

    if save:
        plt.savefig('../compte_rendu_images/matplotlib/{}.png'.format(title),bbox_inches = 'tight',pad_inches = 0)

    plt.show()

    

def plot_hist(im,save=False,name=''):
    m = np.amin(im)
    M = np.amax(im)
    std = np.std(im)
    mean = np.mean(im)

    plt.figure()
    plt.hist(np.ravel(im),bins='auto',density=True)  
    plt.title('min : {:.3}. max : {:.3}. std : {:.3}. mean : {:.3}'.format(m,M,std,mean))
    if save:
        plt.savefig('../compte_rendu_projet/hist/{}.png'.format(name),bbox_inches = 'tight',pad_inches = 0)
    plt.show()
    
        
def denormalize(im,im_noisy,mean_correction=5.47):
    # m = -1.16
    # M = 12.15
    # mean_correction = 5.15
    # mean_correction = 5.47
    
    mean = np.mean(np.log(np.abs(im_noisy)+np.spacing(1)))
    im_ = np.multiply(im,np.exp(mean-mean_correction))
    return im_
    
def plot_im_cv(img,title = ''):
    cv2.imshow(title,img)
       
def normalize_sar(im,m,M,mean_correction):
    log_im = np.log(im+np.spacing(1))
    mean = np.mean(log_im)
    norm = (log_im-mean+mean_correction-m)/(M-m)
    norm = np.clip(norm,0,1)
    return (norm).astype(np.float32)

class normalization():
    def __init__(self,m,M) -> None:

        self.m_ = m
        self.M_ = M
        
    def __call__(self,im):
        
        assert isinstance(im,np.ndarray)
        log_im = np.log(im+10**(-20))
        num = log_im - self.m_
        den = self.M_-self.m_
        norm = num/den
        return (norm).astype(np.float32)
    
def standard_norm(im):
    log_im = np.log(im+np.spacing(1))
    m = -1.429329123112601
    M = 10.089038980848645
    im_f = ((log_im - m) / (M - m)).astype(np.float32)
    return im_f 
    
def save_im(im,fold,is_sar=True,tresh=None):

    im = np.abs(im)
    shape_im = im.shape

    if len(shape_im) == 2:
        polsar_im = im
    elif len(shape_im) == 3:
        if shape_im[2] == 4:
            polsar_im = np.zeros((shape_im[0],shape_im[1],3),dtype=np.single)
            polsar_im[:,:,0] = im[:,:,0]
            polsar_im[:,:,1] = (im[:,:,1]+im[:,:,2])/2
            polsar_im[:,:,2] = im[:,:,3]
        elif shape_im[2] != 3:
            print('Number of channel should be 3 or 4 but is {}'.format(shape_im[2]))
            return 
    else :
        name = os.path.basename(fold)
        print('can''t save {} because it is not an image'.format(name))
        return

    if is_sar:
        polsar_im = tresh_im(polsar_im,treshold=tresh)*255
    else :
        polsar_im = normalize01(polsar_im)*255
    cv2.imwrite(fold, polsar_im)


def disp_sar(im,tresh=None, ch=None, title = ''):
    im = np.abs(im)
    shape_im = im.shape

    # Take one chanel
    if ch is not None:
        im = im[:,:,ch]

    # Create RGB image with G = 1/2(hv+vh)
    elif len(shape_im) == 3 and shape_im[2] == 4:
        polsar_im = np.zeros((shape_im[0],shape_im[1],3),dtype=np.single)
        polsar_im[:,:,0] = im[:,:,0]
        polsar_im[:,:,1] = (im[:,:,1]+im[:,:,2])/2
        polsar_im[:,:,2] = im[:,:,3]
        im = polsar_im

    # Apply treshold to image
    if tresh == None:
        im_t = tresh_im(im)
    else:
        im_t = tresh_im(im,treshold=tresh)
    
    # Display in BGR style to have same image as opencv imwrite
    if len(shape_im) == 3 and shape_im[2] == 4:
        im_t = im_t[:,:,::-1]

    plot_im(im_t, title = title)


if __name__ == "__main__":
    
    pass


