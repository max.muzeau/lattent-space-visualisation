from tkinter.tix import Tree
import numpy as np
from utils import *
import matplotlib.pyplot as plt
import cv2
import glob


   
def crop_result_ano():

    # PATH 
    #-----------------------

    folder = '/home/max/Documents/pipeline_anomaly_detection/data/compte_rendu_images/foret'
    save_folder = '/home/max/Documents/pipeline_anomaly_detection/data/compte_rendu_images/foret_crop'

    folder = '/home/max/Documents/pipeline_anomaly_detection/data/compte_rendu_images/pont_cible'
    save_folder = '/home/max/Documents/pipeline_anomaly_detection/data/compte_rendu_images/pont_cible_crop'

    n_crop = '0'

    folder_paths = glob.glob(folder+'/*.npy')

    input_paths = filter(lambda path : 'input' in path, folder_paths)
    output_paths = filter(lambda path : 'reconstruction' in path, folder_paths)
    ano_paths = filter(lambda path : 'ano_score' in path, folder_paths)

    

    # SAVE 
    #-----------------------
    polar_tresh = [4.585E-8,1.651E-8,6.102E-8]

    save = False
    save_crop = False

    if save or save_crop :
        os.makedirs(save_folder,exist_ok=True)

    # CROP REGION 
    #-----------------------
    patch_size = 512

    crop0 = [450,700] #foret
    crop1 = [350,500] #pont_cible

    crop = crop1

    
    # # PLOT input
    # #------------------------
    # for path in input_paths :

    #     im = np.load(path)
    #     name = os.path.splitext(os.path.basename(path))[0]
    #     sub_im = im[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size,:]
    #     disp_sar(im,tresh = polar_tresh,title=name)
    #     disp_sar(sub_im,tresh = polar_tresh,title='{}_crop{}'.format(name,n_crop))

    #     if save :
    #         print('saving image : {}'.format(name))
    #         save_im(im,'{}/{}.png'.format(save_folder,name),is_sar=True,tresh=polar_tresh)
            
    #     if save_crop :
    #         print('saving image : {} crop : {}'.format(name,n_crop))
    #         save_im(sub_im,'{}/{}_crop{}.png'.format(save_folder,name,n_crop),is_sar=True,tresh=polar_tresh)

    # # PLOT output
    # #------------------------
    # for path in output_paths :

    #     im = np.load(path)
    #     name = os.path.splitext(os.path.basename(path))[0]
    #     sub_im = im[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size,:]
    #     disp_sar(im,tresh = polar_tresh,title=name)
    #     disp_sar(sub_im,tresh = polar_tresh,title='{}_crop{}'.format(name,n_crop))

    #     if save :
    #         print('saving image : {}'.format(name))
    #         save_im(im,'{}/{}.png'.format(save_folder,name),is_sar=True,tresh=polar_tresh)
            
    #     if save_crop :
    #         print('saving image : {} crop : {}'.format(name,n_crop))
    #         save_im(sub_im,'{}/{}_crop{}.png'.format(save_folder,name,n_crop),is_sar=True,tresh=polar_tresh)

    
    # PLOT ano
    #------------------------
    min_db = -110
    for path in ano_paths :

        im = np.load(path)
        name = os.path.splitext(os.path.basename(path))[0]
        sub_im = im[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size]

        # compute ano score in dB
        log_im = np.clip(20*np.log(normalize01(im+10**(-15),val =[0,np.amax(im)])),min_db,0)
        log_sub_im = np.clip(20*np.log(normalize01(sub_im+10**(-15),val =[0,np.amax(im)])),min_db,0)
        
        plot_im(log_im,title=name,bar=True)
        plot_im(log_sub_im,title=name,bar=True)
       
        if save :
            print('saving image : {}'.format(name))
            save_im(log_im,'{}/{}.png'.format(save_folder,name),is_sar=False,save = True)
            
        if save_crop :
            print('saving image : {} crop : {}'.format(name,n_crop))
            save_im(log_sub_im,'{}/{}_crop{}.png'.format(save_folder,name,n_crop),is_sar=False)

    

def crop_result_denoise():

    # PATH 
    #-----------------------

    input_data = '../denoised_tsx/d_dom.npy'
    output_data = '../denoised_tsx/d_dom.npy'

    fold = './'
    weights = 'test1'
    n_crop = '1'

    # SAVE 
    #-----------------------

    save_input = 0
    save_output = 0

    save_input_sub = 0
    save_output_sub = 0

    save_ratio = 0
    save_ratio_sub = 0

    # CROP REGION 
    #-----------------------
    patch_size = 128

    crop1 = [50,670]
    crop2 = [790,210]
    crop3 = [0,0]

    crop = crop2

    # LOAD - CROP - RATIO
    #-----------------------

    input_0 = np.abs(np.load(input_data))
    output_0 = np.abs(np.load(output_data))
    ratio_0 = np.divide(input_0,output_0+np.spacing(1))

    input = tresh_im(input_0,treshold=430)
    output = tresh_im(output_0,treshold=430)
    ratio = tresh_im(ratio_0,treshold=3)

    sub_input = input[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size]
    sub_output = output[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size]
    sub_ratio = ratio[crop[0]:crop[0]+patch_size,crop[1]:crop[1]+patch_size]

    # PLOT denoised im
    #------------------------
    plot_im(input)
    plot_im(output)

    if save_input==1:
        print('\nSAVED INPUT')
        save_im(input,'{}noisy.png'.format(fold))
        
    if save_output==1:
        print('\nSAVED OUTPUT')
        save_im(output,'{}{}_output.png'.format(fold,weights))

    # PLOT noisy subim
    #------------------------

    plot_im(sub_input)

    if save_input_sub==1:
        print('\nSAVED INPUT SUB')
        save_im(sub_input,'{}input_sub_{}.png'.format(fold,n_crop))

    # PLOT denoised subim
    #------------------------

    plot_im(sub_output)

    if save_output_sub==1:
        print('\nSAVED OUTPUT SUB')
        save_im(sub_output,'{}{}_{}_denoised_sub.png'.format(fold,weights,n_crop))
        
    # RATIO
    #------------------------

    plot_im(sub_ratio)

    if save_ratio_sub==1:
        print('\nSAVED RATIO SUB')
        save_im(sub_ratio,'{}{}_{}_ratio_sub.png'.format(fold,weights,n_crop))
        
    if save_ratio==1:
        print('\nSAVED RATIO')
        save_im(ratio,'{}{}_ratio.png'.format(fold,weights))


if __name__ == '__main__':
    crop_result_ano()

