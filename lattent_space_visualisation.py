from __future__ import print_function
import glob
import os
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import time
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

def create_data(dir):
    files_path = glob.glob('{}/*.npy'.format(dir))
    for n,file_path in enumerate(files_path):
        vectors = np.load(file_path)
        name = os.path.splitext(os.path.basename(file_path))[0]
        if n == 0:
            feat_cols = [ 'feature_'+str(i) for i in range(vectors.shape[1]) ]

        if 'champ_' in name:
            label_n = 1
            name = 'no_ano'
        else:
            label_n = 2
            name = 'ano'

        label = (np.ones((vectors.shape[0]))*label_n).astype(int)
        df_n = pd.DataFrame(vectors,columns=feat_cols)
        df_n['label'] = label
        df_n['name'] = [name for i in range(vectors.shape[0])]
        if n==0:
            df = df_n
        else:
            df = pd.concat([df,df_n])
    return df

def compress_data(data:pd.DataFrame,nb_pca:int,nb_tsne:int):

    # PCA
    pca = PCA(n_components=nb_pca)
    pca_result = pca.fit_transform(data.filter(like='feature_',axis=1).values)
    print('Cumulative explained variation for {} principal components: {}'.format(nb_pca,np.sum(pca.explained_variance_ratio_)))
    # t-SNE
    time_start = time.time()
    tsne = TSNE(n_components=nb_tsne, verbose=0, perplexity=40, n_iter=300)
    tsne_pca_results = tsne.fit_transform(pca_result)
    print('t-SNE done with {} components. Time elapsed: {} seconds'.format(nb_tsne,time.time()-time_start))
    for i in range(nb_tsne):
        data['axis_{}'.format(i)] = tsne_pca_results[:,i]

def visualisation(data):

    dim = len(list(filter(lambda col: 'axis_' in col,list(df.columns))))
    if dim not in [2,3]:
        print('Dimension must be 2 or 3')
        return
    nb_label = len(pd.unique(data['label']))

    if dim == 2:
        plt.figure(figsize=(16,10))
        sns.scatterplot(
            x="axis_0", y="axis_1",
            hue="name",
            palette=sns.color_palette("hls", nb_label),
            data=data,
            legend="full",
            alpha=0.7
        )
        plt.show()
    elif dim == 3:
        # Visualisation 3D
        ax = plt.figure(figsize=(16,10)).gca(projection='3d')
        ax.scatter(
            xs=data["axis_0"], 
            ys=data["axis_1"], 
            zs=data["axis_2"], 
            c=data["label"]
        )
        ax.set_xlabel('axis_0')
        ax.set_ylabel('axis_1')
        ax.set_zlabel('axis_2')
        plt.show()



if __name__ == '__main__':

    df = create_data('./latt_space/disc')
    compress_data(df,20,2)
    visualisation(df)




