from cProfile import label
import sys
sys.path.append('/media/max/TOSHIBA EXT/ONERA_SONDRA/algos/git_AAE')
from pipeline.storage.state import StateStorageFile
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("whitegrid")

def x_lr_y_loss(dic):
    lr = dic.get_value('lr')
    for key in dic._state:
        if key != 'lr':
            l = dic.get_value(key)
            plt.plot(lr,l,label=key)
    plt.legend()
    plt.show()

def x_step_y_loss(dic):
    for key in dic._state:
        if key != 'lr':
            l = dic.get_value(key)
            if 'step' not in locals():
                step = list(range(1,len(l)+1)) 
            plt.plot(step,l,label=key)
    plt.legend()
    plt.show()

def plot_lr(dic):
    lr = dic.get_value('lr')
    plt.plot(lr)
    plt.show()

def comp_rec_loss(dic1,dic2,label1,label2):
    rec1 = dic1.get_value('rec')
    rec2 = dic2.get_value('rec')
    step = list(range(1,len(rec1)+1)) 


    plt.plot(step,rec1,label=label1)
    plt.plot(step,rec2,label=label2)
    plt.legend()
    plt.show()



if __name__ == "__main__":

    dic = StateStorageFile('./pipeline/out/AAE_CLR/loss_lr')
    dic2 = StateStorageFile('./pipeline/out/AAE_CLR_no_disc/loss_lr')

    comp_rec_loss(dic,dic2,'with discriminator','without discriminator')
